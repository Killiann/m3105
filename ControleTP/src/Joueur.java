
public class Joueur implements Cloneable {
	
	private String nom;
	private String prénom;
	private Email email;
	
	public Joueur(String nom, String prénom, Email email) {
		this.nom = nom;
		this.prénom = prénom;
		this.email = email;
	}

	// Question 3: On ajoute un setter
	public void setNom(String nom) {
		this.nom = nom;
	}

	public Email getEmail() {
		return email;
	}

	public void setPrénom(String prénom) {
		this.prénom = prénom;
	}

	public void setEmail(Email email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "Joueur : "+ nom + ' ' +  prénom + ", " + email;
	}

	@Override
	protected Joueur clone() {
		try {
			Joueur joueur = (Joueur) super.clone();
			joueur.email = this.email.clone();
			return joueur;
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		return null;
	}
}

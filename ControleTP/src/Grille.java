import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class Grille implements Cloneable {
	
	private static float[][] prixGrilleMultiple = {{ 2.2F,  13.2F,  46.2F, 123.2F, 277.2F},
												   { 4.4F,  26.4F,  92.4F, 246.4F, Float.MAX_VALUE},
												   { 6.6F,  39.6F, 138.6F, 369.6F, Float.MAX_VALUE},
												   { 8.8F,  52.8F, 184.8F, Float.MAX_VALUE, Float.MAX_VALUE},
												   {  11F,  66.0F, 231.0F, Float.MAX_VALUE, Float.MAX_VALUE},
												   {13.2F,  79.2F, 277.2F, Float.MAX_VALUE, Float.MAX_VALUE},
												   {15.4F,  92.4F, 323.4F, Float.MAX_VALUE, Float.MAX_VALUE},
												   {17.6F, 105.6F, 369.6F, Float.MAX_VALUE, Float.MAX_VALUE},
												   {19.8F, 118.8F, Float.MAX_VALUE, Float.MAX_VALUE, Float.MAX_VALUE},
												   {22.0F, 132.0F, Float.MAX_VALUE, Float.MAX_VALUE, Float.MAX_VALUE}};
	
	private List<Integer> numérosGrille = new LinkedList<Integer>();
	private List<Integer> numérosComplémentaires = new LinkedList<Integer>();
	
	public void updateGrilleNumero(Integer... numérosGrille) throws IllegalArgumentException {
		if (numérosGrille.length < 5 || numérosGrille.length > 9)
			throw new IllegalArgumentException("Vous devez saisir entre 5 et 9 numéros");
		Arrays.sort(numérosGrille);
		if (deuxNumérosIdentiques(numérosGrille))
			throw new IllegalArgumentException("Pas deux fois le méme numéro !");
		if (auMoinsUnNuméroHorsLimite(1, 49, numérosGrille))
			throw new IllegalArgumentException("Numéros d'une grille compris entre 1 et 10");
		this.numérosGrille = Arrays.asList(numérosGrille);
	}
	
	public void updateGrilleComplémentaire(Integer... numérosComplémentaires) {
		if (numérosComplémentaires.length < 1 || numérosComplémentaires.length > 10)
			throw new IllegalArgumentException("Vous devez saisir entre 1 et 10 numéros complémentaires");
		Arrays.sort(numérosComplémentaires);
		if (deuxNumérosIdentiques(numérosComplémentaires))
			throw new IllegalArgumentException("Pas deux fois le méme numéro !");
		if (auMoinsUnNuméroHorsLimite(1, 10, numérosComplémentaires))
			throw new IllegalArgumentException("Numéros complémentaires compris entre 1 et 10");
		this.numérosComplémentaires = Arrays.asList(numérosComplémentaires);
	}
	
	private static boolean deuxNumérosIdentiques(Integer... listeNuméros) {
		for (int i = 1; i < listeNuméros.length; i++) {
			if (listeNuméros[i] == listeNuméros[i-1])
				return true;
		}
		return false;
	}
	
	private static boolean auMoinsUnNuméroHorsLimite(int plusPetit, int plusGrand, Integer... listeNuméros) {
		return (listeNuméros[0] < plusPetit || listeNuméros[listeNuméros.length-1] > plusGrand);
	}
	
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("grille : ");
		for(Integer numéro : this.numérosGrille) {
			sb.append(numéro.toString());
			sb.append(' ');
		}
		sb.append("numéro(s) complémentaires : ");
		for(Integer numéro : this.numérosComplémentaires) {
			sb.append(numéro.toString());
			sb.append(' ');
		}
		return sb.toString();
	}
	
	public float prix() {
		return prixGrilleMultiple[numérosGrille.size()-5][numérosComplémentaires.size()-1];
	}

	@Override
	protected Grille clone() {
		try {
			Grille grille = (Grille) super.clone();
			grille.numérosGrille = new LinkedList<>(this.numérosGrille);
			grille.numérosComplémentaires = new LinkedList<>(this.numérosComplémentaires);
			return grille;
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		return null;
	}
}

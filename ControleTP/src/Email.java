
public class Email implements Cloneable {
	
	private static final String AT = " at ";
	
	private String identifiant;
	private String organisation;
	
	public Email(String identifiant, String organisation) {
		this.identifiant = identifiant;
		this.organisation = organisation;
	}

	public void setOrganisation(String organisation) {
		this.organisation = organisation;
	}

	//Question 3: On ajoute un setter
	public void setIdentifiant(String identifiant) {
		this.identifiant = identifiant;
	}

	@Override
	public String toString() {
		return "Email : " + identifiant + AT + organisation;
	}

	@Override
	protected Email clone() {
		try {
			return (Email) super.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		return null;
	}
}


public class DateTirage implements Cloneable {

	// Cette class peut etre un multiton car on pourrait faire une Map<String, DateTirage> ou la clé est un String et la valeur
	// l'instance de cette class
	// Ca permet d'éviter de défirer plein de variable static et donc ça refractor le code

	private int jour;
	private int mois;
	private int année;
	private String toString;
	
	public static DateTirage premierOctobre = new DateTirage(1,10,2021, "Vendredi 1er Octobre 2021");
	public static DateTirage quatreOctobre = new DateTirage(4,10,2021, "Lundi 4 Octobre 2021");
	public static DateTirage sixOctobre = new DateTirage(6,10,2021, "Mercredi 6 Octobre 2021");
	public static DateTirage huitOctobre = new DateTirage(8,10,2021, "Vendredi 8 Octobre 2021");
	public static DateTirage OnzeOctobre = new DateTirage(11,10,2021, "Lundi 11 Octobre 2021");
	public static DateTirage TreizeOctobre = new DateTirage(13,10,2021, "Mercredi 13 Octobre 2021");
	public static DateTirage QuinzeOctobre = new DateTirage(15,10,2021, "Vendredi 15 Octobre 2021");
	public static DateTirage vingtcinqOctobre = new DateTirage(25,10,2021, "Lundi 25 Octobre 2021");
	public static DateTirage vingtseptOctobre = new DateTirage(27,10,2021, "Mercredi 27 Octobre 2021");
	public static DateTirage vingtneufOctobre = new DateTirage(29,10,2021, "Vendredi 29 Octobre 2021");
	
	private DateTirage(int jour, int mois, int année, String toString) {
		this.jour = jour;
		this.mois = mois;
		this.année = année;
		this.toString = toString;
	}

	public String toString() {
		return "Date tirage : " + this.toString;
	}

	// On rajoute le clone par sécurité au cas ou les variables static au dessus sont changé
	// Comme elles ont pas de final, ce ne sont pas des constantes
	@Override
	protected DateTirage clone() {
		try {
			return (DateTirage) super.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		return null;
	}
}

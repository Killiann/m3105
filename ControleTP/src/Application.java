public class Application {

    public static void main(String[] args) {
        // Je joue le premier lundi et mercredi toujours la méme grille
        // mais le mercredis mon adresse mail change
        Joueur Hervé = new Joueur("Leblanc", "Hervé", new Email("Herve.Leblanc", "iut-tlse3.fr"));
        Grille maGrille = new Grille();
        maGrille.updateGrilleNumero(7,10,6,2,18);
        maGrille.updateGrilleComplémentaire(7);


        Jeu premierJeu = new Jeu(Hervé, maGrille, DateTirage.quatreOctobre);

        Jeu deuxiémeJeu = premierJeu.clone();
        deuxiémeJeu.getUnJoueur().getEmail().setOrganisation("irit.fr");
        deuxiémeJeu.setUneDateDeTirage(DateTirage.sixOctobre);


        // mon fils Arthur joue les mémes numéros que moi les lundi et mercredi de la semaine suivante
        // il rajoute deux numéros complémentaires dur ces grilles.
        Jeu troisiémeJeu = deuxiémeJeu.clone();
        troisiémeJeu.getUnJoueur().setPrénom("Arthur");
        troisiémeJeu.getUnJoueur().setEmail(new Email("Arthur.theBest","gmail.com"));
        troisiémeJeu.getUneGrille().updateGrilleComplémentaire(7,1,2);
        troisiémeJeu.setUneDateDeTirage(DateTirage.OnzeOctobre);



        Jeu quatriémeJeu = troisiémeJeu.clone();
        quatriémeJeu.setUneDateDeTirage(DateTirage.QuinzeOctobre);


        // Puis il devient accroc et décide de tenter une derniére fois sa chance
        // avec le maximum de numéros de grilles possibles, cela va lui coéter cher !
        Jeu cinquiémeJeu = quatriémeJeu.clone();
        cinquiémeJeu.getUneGrille().updateGrilleNumero(7,10,6,2,18,23,24,25,33);
        cinquiémeJeu.setUneDateDeTirage(DateTirage.vingtcinqOctobre);


        // impressions écran
        System.out.println(premierJeu);
        System.out.println(deuxiémeJeu);
        System.out.println(troisiémeJeu);
        System.out.println(quatriémeJeu);
        System.out.println(cinquiémeJeu);
    }

}

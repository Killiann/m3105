public class Jeu implements Cloneable {
	
	private Joueur unJoueur;
	private Grille uneGrille;
	private DateTirage uneDateDeTirage;
	
	public Jeu(Joueur unJoueur, Grille uneGrille, DateTirage uneDateDeTirage) {
		this.unJoueur = unJoueur;
		this.uneGrille = uneGrille;
		this.uneDateDeTirage = uneDateDeTirage;
	}
	
	public String toString() {
		return unJoueur.toString() + '\n' +  uneDateDeTirage.toString() + '\n' +  uneGrille.toString() + '\n' + "Prix é payer : " + uneGrille.prix() + " é";
	}

	public Joueur getUnJoueur() {
		return unJoueur;
	}

	//Question 3: on ajoute un setter
	public void setUnJoueur(Joueur unJoueur) {
		this.unJoueur = unJoueur;
	}

	public void setUneDateDeTirage(DateTirage uneDateDeTirage) {
		this.uneDateDeTirage = uneDateDeTirage;
	}

	public Grille getUneGrille() {
		return uneGrille;
	}

	@Override
	protected Jeu clone() {
		try {
			Jeu jeu = (Jeu) super.clone();
			jeu.unJoueur = this.unJoueur.clone();
			jeu.uneGrille = this.uneGrille.clone();
			jeu.uneDateDeTirage = this.uneDateDeTirage.clone(); // La raison est exploiqué sur le clone
			return jeu;
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		return null;
	}
}

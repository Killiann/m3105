public class MonApplication {

    public static void main(String[] args) {
        // Nous repronons la même base que Application.java en changeant juste quelques valeurs
        Grille maGrille = new Grille();
        maGrille.updateGrilleNumero(5,3,1,2,18);
        maGrille.updateGrilleComplémentaire(8);

        // Monsieur Leblanc va continuer à jouer comme il est est devenu accroc comme son fils
        Joueur Hervé = new Joueur("Leblanc", "Hervé", new Email("Herve.Leblanc", "iut-tlse3.fr"));

        // Je vais également jouer
        Joueur killian = Hervé.clone();
        killian.getEmail().setIdentifiant("killian.falguiere");
        killian.getEmail().setOrganisation("gmail.com");
        killian.setNom("Falguiere");
        killian.setPrénom("Killian");

        //Nous allons maintenant créer les jeux
        Jeu premierJeuHervé = new Jeu(Hervé, maGrille, DateTirage.quatreOctobre);

        // Monsieur veut vraiment gagner vite alors il joue une deuxieme grille 2 jours après. Cependant il ne change pas son numéro
        // fétiche le 8
        Jeu deuxiemeJeuHervé = premierJeuHervé.clone();
        deuxiemeJeuHervé.getUneGrille().updateGrilleNumero(1, 2, 3, 4, 5);
        deuxiemeJeuHervé.setUneDateDeTirage(DateTirage.sixOctobre);

        // C'est a mon tour de faire des jeux, comme je n'ai pas de personnalité je vais faire comme mon professeur en jouant 2 fois
        Jeu premierJeuKillian = deuxiemeJeuHervé.clone();
        premierJeuKillian.setUnJoueur(killian.clone());
        premierJeuKillian.setUneDateDeTirage(DateTirage.quatreOctobre);
        // je vais changer que ma grille complémentaire car j'ai confiance en mon professeur
        premierJeuKillian.getUneGrille().updateGrilleComplémentaire(6, 5, 4);

        // On a passé le 6 octobre et Monsieur leblanc n'est toujours pas millionaire donc j'attends un peu et je rejous
        Jeu deuxiemeJeuKillian = premierJeuKillian.clone();
        deuxiemeJeuKillian.setUneDateDeTirage(DateTirage.vingtneufOctobre);
        deuxiemeJeuKillian.getUneGrille().updateGrilleNumero(9, 8, 7, 6, 5, 4, 3, 2, 1); // Je change ma grille

        // impressions écran
        System.out.println(premierJeuHervé);
        System.out.println(deuxiemeJeuHervé);
        System.out.println(premierJeuKillian);
        System.out.println(deuxiemeJeuKillian);
    }
}

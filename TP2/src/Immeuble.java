import java.util.Objects;

public class Immeuble extends ObjetGrahique {

    private String couleur;
    private int nbEtages;
    private int hauteurEtage;

    public Immeuble(double coordX, double coordY, String couleur, int nbEtages, int hauteurEtage) {
        super(coordX, coordY);
        this.couleur = couleur;
        this.nbEtages = nbEtages;
        this.hauteurEtage = hauteurEtage;
    }

    public void setCouleur(String couleur) {
        this.couleur = couleur;
    }

    public void setNbEtages(int nbEtages) {
        this.nbEtages = nbEtages;
    }

    public void setHauteurEtage(int hauteurEtage) {
        this.hauteurEtage = hauteurEtage;
    }

    @Override
    public String toString() {
        return super.toString() + "Immeuble{" +
                "couleur='" + couleur + '\'' +
                ", nbEtages=" + nbEtages +
                ", hauteurEtage=" + hauteurEtage +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Immeuble immeuble = (Immeuble) o;
        return nbEtages == immeuble.nbEtages &&
                hauteurEtage == immeuble.hauteurEtage &&
                Objects.equals(couleur, immeuble.couleur);
    }
}

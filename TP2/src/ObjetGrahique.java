public class ObjetGrahique implements Cloneable {

    private double coordX;
    private double coordY;

    public ObjetGrahique(double coordX, double coordY) {
        this.coordX = coordX;
        this.coordY = coordY;
    }

    public void translater(double dx, double dy) {
        coordX+=dx;
        coordY+=dy;
    }

    @Override
    public String toString() {
        return super.toString() + "ObjetGrahique{" +
                "coordX=" + coordX +
                ", coordY=" + coordY +
                '}';
    }

    @Override
    public ObjetGrahique clone() {
        try {
            return (ObjetGrahique) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return null;
    }
}

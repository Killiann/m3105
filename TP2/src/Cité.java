import java.util.LinkedList;

public class Cité implements Cloneable {

    private String name;
    private LinkedList<ObjetGrahique> objetGrahiques = new LinkedList<>();

    public Cité(String name) {
        this.name = name;
    }

    public LinkedList<ObjetGrahique> getData() {
        return objetGrahiques;
    }

    @Override
    protected Cité clone() {
        try {
            Cité cité = (Cité) super.clone();
            cité.objetGrahiques = new LinkedList<>(objetGrahiques);
            return cité;
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return null;
    }
}

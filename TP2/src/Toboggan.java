public class Toboggan extends ObjetGrahique {

    private String couleur;
    private int hauteur;
    private int largeur;

    public Toboggan(double coordX, double coordY, String couleur, int hauteur, int largeur) {
        super(coordX, coordY);
        this.couleur = couleur;
        this.hauteur = hauteur;
        this.largeur = largeur;
    }

    public void setCouleur(String couleur) {
        this.couleur = couleur;
    }

    public void setHauteur(int hauteur) {
        this.hauteur = hauteur;
    }

    public void setLargeur(int largeur) {
        this.largeur = largeur;
    }

    @Override
    public String toString() {
        return super.toString() + "Toboggan{" +
                "couleur='" + couleur + '\'' +
                ", hauteur=" + hauteur +
                ", largeur=" + largeur +
                '}';
    }
}

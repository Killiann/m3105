
public class Patronyme implements Cloneable{
	
	private String nom;
	private String prenom;
	
	public Patronyme(String nom, String prenom) {
		this.nom = nom;
		this.prenom = prenom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	@Override
	public String toString() {
		return "Patronyme [nom=" + nom + ", prenom=" + prenom + "]";
	}
	
	public Patronyme clone() {
		Patronyme cloned = null;
		try {
			cloned = (Patronyme) super.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		return cloned;
	}
	

}

import java.util.List;

public class Catalogue {

    private static Catalogue instance;

    private Immeuble unImmeuble;
    private Arbre unArbre;
    private Banc unBanc;
    private Balançoire unBalançoire;
    private Toboggan unToboggan;
    private Tourniquet unTourniquet;
    private Cité uneCité;

    private Catalogue() {
        unImmeuble = new Immeuble(0.0, 0.0, "Blanc", 4, 3);
        unArbre = new Arbre(0.0, 0.0, 8, "Brun", "Vert");
        unBalançoire = new Balançoire(0.0, 0.0, "Vert", 3, 2);
        unBanc = new Banc(0.0, 0.0, "Brun", 1, 2);
        unToboggan = new Toboggan(0.0, 0.0, "Jaune", 2, 4);
        unTourniquet = new Tourniquet(0.0, 0.0, "Rouge", 1, 3);
        uneCité = new Cité("cité des alouettes");
        initializeCite();
    }

    private void initializeCite() {
        List<ObjetGrahique> data = uneCité.getData();
        data.add(unImmeuble.clone());
        data.add(unImmeuble.clone());
        data.add(unBanc.clone());
        data.add(unBanc.clone());
        data.add(unBanc.clone());
        data.add(unBalançoire.clone());
        data.add(unToboggan.clone());
        data.add(unArbre.clone());
        data.add(unArbre.clone());
    }

    public Immeuble getUnImmeuble() {
        return (Immeuble) unImmeuble.clone();
    }

    public Arbre getUnArbre() {
        return (Arbre) unArbre.clone();
    }

    public Banc getUnBanc() {
        return (Banc) unBanc.clone();
    }

    public Balançoire getUnBalançoire() {
        return (Balançoire) unBalançoire.clone();
    }

    public Toboggan getUnToboggan() {
        return (Toboggan) unToboggan.clone();
    }

    public Tourniquet getUnTourniquet() {
        return (Tourniquet) unTourniquet.clone();
    }

    public Cité getUneCité() {
        return uneCité.clone();
    }

    public static Catalogue getInstance() {
        return instance == null ? instance = new Catalogue() : instance;
    }
}

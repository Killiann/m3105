
public class ClientPersonne {

	public static void main(String[] args) {
		Personne Herve = new Personne(58,new Patronyme("Leblanc","Hervé"));
		Personne Arthur = Herve.clone();
		Arthur.getPatronyme().setPrenom("Arthur");
		Arthur.setAge(30);
		Personne Vincent = Arthur.clone();
		Vincent.getPatronyme().setPrenom("Vincent");
		System.out.println(Herve);
		System.out.println(Arthur);
		System.out.println(Vincent);

	}

}

public class CiteMain {

    public static void main(String[] args) {
        Catalogue catalogue = Catalogue.getInstance();
        Cité uneCité = catalogue.getUneCité();
        Immeuble test = (Immeuble) uneCité.getData().get(0);
        test.setCouleur("Rouge");
        test.translater(3.3, 4.4);
        System.out.println(uneCité);
        Cité uneAutreCité = catalogue.getUneCité();
        System.out.println(uneAutreCité);

        uneCité.getData().remove(0);
        System.out.println(uneCité.getData().size());
        System.out.println(uneAutreCité.getData().size());
    }
}

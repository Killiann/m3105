import java.util.Objects;

public class Arbre extends ObjetGrahique {

    private int hauteur;
    private String couleurTronc;
    private String couleurFeuilles;

    public Arbre(double coordX, double coordY, int hauteur, String couleurTronc, String couleurFeuilles) {
        super(coordX, coordY);
        this.hauteur = hauteur;
        this.couleurTronc = couleurTronc;
        this.couleurFeuilles = couleurFeuilles;
    }

    public void setHauteur(int hauteur) {
        this.hauteur = hauteur;
    }

    public void setCouleurTronc(String couleurTronc) {
        this.couleurTronc = couleurTronc;
    }

    public void setCouleurFeuilles(String couleurFeuilles) {
        this.couleurFeuilles = couleurFeuilles;
    }

    @Override
    public String toString() {
        return super.toString() + "Arbre{" +
                "hauteur=" + hauteur +
                ", couleurTronc='" + couleurTronc + '\'' +
                ", couleurFeuilles='" + couleurFeuilles + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Arbre arbre = (Arbre) o;
        return hauteur == arbre.hauteur &&
                Objects.equals(couleurTronc, arbre.couleurTronc) &&
                Objects.equals(couleurFeuilles, arbre.couleurFeuilles);
    }
}

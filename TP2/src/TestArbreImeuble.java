import org.junit.Assert;
import org.junit.Test;

public class TestArbreImeuble {

    @Test
    public void testCloneImmeuble() {
        Immeuble immeuble1 = new Immeuble(1, 1, "vert", 1, 1);
        Immeuble immeuble2 = (Immeuble) immeuble1.clone();

        Assert.assertEquals(immeuble1, immeuble2);
        Assert.assertNotSame(immeuble1, immeuble2);
    }

    @Test
    public void testCloneArbre() {
        Arbre arbre1 = new Arbre(1, 1, 1, "df", "df");
        Arbre arbre2 = (Arbre) arbre1.clone();

        Assert.assertEquals(arbre1, arbre2);
        Assert.assertNotSame(arbre1, arbre2);
    }
}

public class Balançoire extends ObjetGrahique {

    private String couleur;
    private int hauteur;
    private int nbSieges;

    public Balançoire(double coordX, double coordY, String couleur, int hauteur, int nbSieges) {
        super(coordX, coordY);
        this.couleur = couleur;
        this.hauteur = hauteur;
        this.nbSieges = nbSieges;
    }

    public void setCouleur(String couleur) {
        this.couleur = couleur;
    }

    public void setHauteur(int hauteur) {
        this.hauteur = hauteur;
    }

    public void setNbSieges(int nbSieges) {
        this.nbSieges = nbSieges;
    }

    @Override
    public String toString() {
        return super.toString() + "Balançoire{" +
                "couleur='" + couleur + '\'' +
                ", hauteur=" + hauteur +
                ", nbSieges=" + nbSieges +
                '}';
    }
}

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PremierPaysage {
    public static void main(String[] args) {
        Catalogue catalogue = Catalogue.getInstance();
        List<ObjetGrahique> paysage = new ArrayList<>(Arrays.asList(catalogue.getUnImmeuble(),
                catalogue.getUnArbre(),
                catalogue.getUnBanc(),
                catalogue.getUnBalançoire(),
                catalogue.getUnToboggan(),
                catalogue.getUnTourniquet()));

        for (ObjetGrahique o : new ArrayList<>(paysage)) {
            o.translater(Math.random() * 10, Math.random() * 10);

            ObjetGrahique clone = o.clone();
            clone.translater(Math.random() * 10, Math.random() * 10);
            paysage.add(clone);
        }

        System.out.println(paysage);
    }
}

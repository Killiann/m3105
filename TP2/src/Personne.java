
public class Personne implements Cloneable {
	
	private int age;
	private Patronyme patronyme;
	
	public Personne(int age, Patronyme patronyme) {
		this.age = age;
		this.patronyme = patronyme;
	}

	public Patronyme getPatronyme() {
		return patronyme;
	}

	public void setAge(int age) {
		this.age = age;
	}

	@Override
	public String toString() {
		return "Personne [age=" + age + ", patronyme=" + patronyme + "]";
	}
	
	public Personne clone() {
		Personne cloned = null;
		try {
			cloned = (Personne) super.clone();
			cloned.patronyme = this.patronyme.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		return cloned;
	}

}

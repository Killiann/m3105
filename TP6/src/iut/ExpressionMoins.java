package iut;

public class ExpressionMoins extends ExpressionArithmetique {

    public ExpressionMoins() {
    }

    @Override
    public int evaluate() {
        if(entiers.isEmpty()) return 0;

        int val = entiers.get(0).evaluate();

        for (int i = 1; i < entiers.size(); i++) {
            val-=entiers.get(i).evaluate();
        }
        return val;
    }
}

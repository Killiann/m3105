package iut;

import java.util.ArrayList;
import java.util.List;

public abstract class ExpressionArithmetique {

    protected List<ExpressionArithmetique> entiers;

    public ExpressionArithmetique() {
        this.entiers = new ArrayList<>();
    }

    public void add(ExpressionArithmetique entier) {
        this.entiers.add(entier);
    }

    public abstract int evaluate();
}

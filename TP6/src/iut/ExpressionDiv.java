package iut;

public class ExpressionDiv extends ExpressionArithmetique {

    public ExpressionDiv() {
    }

    @Override
    public int evaluate() {
        if(this.entiers.isEmpty()) return 0;

        int val = entiers.get(0).evaluate();

        for (int i = 1; i < entiers.size(); i++) {
            try {
                val/=entiers.get(i).evaluate();
            } catch (ArithmeticException exception) {
                return Integer.MAX_VALUE;
            }
        }
        return val;
    }
}

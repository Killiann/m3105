package iut.test;

import iut.Entier;
import iut.ExpressionPlus;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ExpressionPlusTest {

    private ExpressionPlus expressionPlus;

    @Before
    public void setUp() {
        this.expressionPlus = new ExpressionPlus();
    }

    @After
    public void tearThrow() {
        this.expressionPlus = null;
    }

    @Test
    public void testAvec1Entier() {
        this.expressionPlus.add(new Entier(1));

        Assert.assertEquals(1, this.expressionPlus.evaluate());
    }

    @Test
    public void testAvec2Entier() {
        this.expressionPlus.add(new Entier(5));
        this.expressionPlus.add(new Entier(7));

        Assert.assertEquals(12, this.expressionPlus.evaluate());
    }

    @Test
    public void testAvec3Entier() {
        this.expressionPlus.add(new Entier(1));
        this.expressionPlus.add(new Entier(2));
        this.expressionPlus.add(new Entier(-3));

        Assert.assertEquals(0, this.expressionPlus.evaluate());
    }
}
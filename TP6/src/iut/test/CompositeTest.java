package iut.test;

import iut.*;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class CompositeTest {

    private ExpressionArithmetique complete;

    @After
    public void tearThrow() {
        this.complete = null;
    }

    @Test
    public void test1() {
        complete = new ExpressionPlus();
        complete.add(new Entier(2));
        ExpressionArithmetique milieu = new ExpressionDiv();
        complete.add(milieu);
        complete.add(new Entier(7));
        ExpressionArithmetique droitediv = new ExpressionMult();
        milieu.add(droitediv);
        milieu.add(new Entier(6));
        droitediv.add(new Entier(3));
        ExpressionArithmetique soustraction = new ExpressionMoins();
        droitediv.add(soustraction);
        droitediv.add(new Entier(6));
        soustraction.add(new Entier(5));
        soustraction.add(new Entier(2));

        Assert.assertEquals(18, this.complete.evaluate());
    }
}
package iut.test;

import iut.Entier;
import iut.ExpressionMoins;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ExpressionMoinsTest {

    private ExpressionMoins expressionMoins;

    @Before
    public void setUp() {
        this.expressionMoins = new ExpressionMoins();
    }

    @After
    public void tearThrow() {
        this.expressionMoins = null;
    }

    @Test
    public void testAvec1Entier() {
        this.expressionMoins.add(new Entier(1));

        Assert.assertEquals(1, this.expressionMoins.evaluate());
    }

    @Test
    public void testAvec2Entier() {
        this.expressionMoins.add(new Entier(5));
        this.expressionMoins.add(new Entier(7));

        Assert.assertEquals(-2, this.expressionMoins.evaluate());
    }

    @Test
    public void testAvec3Entier() {
        this.expressionMoins.add(new Entier(1));
        this.expressionMoins.add(new Entier(2));
        this.expressionMoins.add(new Entier(-3));

        Assert.assertEquals(2, this.expressionMoins.evaluate());
    }
}
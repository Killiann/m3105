package iut.test;

import iut.Entier;
import iut.ExpressionDiv;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ExpressionDivTest {

    private ExpressionDiv expressionDiv;

    @Before
    public void setUp() {
        this.expressionDiv = new ExpressionDiv();
    }

    @After
    public void tearThrow() {
        this.expressionDiv = null;
    }

    @Test
    public void testAvec1Entier() {
        this.expressionDiv.add(new Entier(1));

        Assert.assertEquals(1, this.expressionDiv.evaluate());
    }

    @Test
    public void testAvec2Entier() {
        this.expressionDiv.add(new Entier(15));
        this.expressionDiv.add(new Entier(5));

        Assert.assertEquals(3, this.expressionDiv.evaluate());
    }

    @Test
    public void testAvec3Entier() {
        this.expressionDiv.add(new Entier(24));
        this.expressionDiv.add(new Entier(2));
        this.expressionDiv.add(new Entier(-3));

        Assert.assertEquals(-4, this.expressionDiv.evaluate());
    }

    @Test
    public void testAvecDiv0() {
        this.expressionDiv.add(new Entier(15));
        this.expressionDiv.add(new Entier(0));

        Assert.assertEquals(Integer.MAX_VALUE, this.expressionDiv.evaluate());
    }
}
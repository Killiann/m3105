package iut.test;

import iut.Entier;
import iut.ExpressionMult;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ExpressionMultTest {

    private ExpressionMult expressionMult;

    @Before
    public void setUp() {
        this.expressionMult = new ExpressionMult();
    }

    @After
    public void tearThrow() {
        this.expressionMult = null;
    }

    @Test
    public void testAvec1Entier() {
        this.expressionMult.add(new Entier(1));

        Assert.assertEquals(1, this.expressionMult.evaluate());
    }

    @Test
    public void testAvec2Entier() {
        this.expressionMult.add(new Entier(5));
        this.expressionMult.add(new Entier(7));

        Assert.assertEquals(35, this.expressionMult.evaluate());
    }

    @Test
    public void testAvec3Entier() {
        this.expressionMult.add(new Entier(1));
        this.expressionMult.add(new Entier(2));
        this.expressionMult.add(new Entier(-3));

        Assert.assertEquals(-6, this.expressionMult.evaluate());
    }
}
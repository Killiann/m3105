package iut.test;

import iut.Entier;
import org.junit.Assert;
import org.junit.Test;

public class EntierTest {

    @Test
    public void testEvaluate() {
        Assert.assertEquals(7, new Entier(7).evaluate());
        Assert.assertEquals(0, new Entier(0).evaluate());
        Assert.assertEquals(-7, new Entier(-7).evaluate());
    }

}
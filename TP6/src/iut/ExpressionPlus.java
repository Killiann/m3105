package iut;

public class ExpressionPlus extends ExpressionArithmetique {

    public ExpressionPlus() {
    }

    @Override
    public int evaluate() {
        int val = 0;

        for (ExpressionArithmetique entier : entiers) {
            val += entier.evaluate();
        }
        return val;
    }
}

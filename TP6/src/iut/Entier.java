package iut;

public class Entier extends ExpressionArithmetique {

    private int entier;

    public Entier(int entier) {
        this.entier = entier;
    }

    @Override
    public int evaluate() {
        return this.entier;
    }
}

package EXO2.Journalisation;

public class CompteBancaire {
	private int numero;
	private double solde;
	private Journal journal;

	public CompteBancaire(int numero) {
		this.numero = numero;
		this.solde = 0.0;
		this.journal = Journal.getInstance();
	}

	public void déposer(double montant) {
		if (montant > 0.0) {
			solde += montant;
			this.journal.ajouterLog("Dépôt de " + montant + "€ sur le compte " + numero + ".");
		} else {
			this.journal.ajouterLog("/!\\ Dépôt d'une valeur négative impossible (" + numero+ ").");
		}
	}
	
	public void retirer(double montant) {
		if (montant > 0.0) {
			if (solde >= montant) {
				solde -= montant;
				this.journal.ajouterLog("Retrait de " + montant + "€ sur le compte " + numero+ ".");
			} else {
				this.journal.ajouterLog("/!\\ La banque n'autorise pas de découvert (" + numero+ ").");
			}
		} else {
			this.journal.ajouterLog("/!\\ Retrait d'une valeur négative impossible (" + numero+ ").");
		}
	}
}

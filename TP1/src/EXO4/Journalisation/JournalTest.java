package EXO4.Journalisation;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class JournalTest {

    @Test
    public void testsComptesBancaires() {
        CompteBancaire cb1 = new CompteBancaire(123456789);
        assertEquals(0,Journal.getOpérations().getLog().length());
        assertEquals(0,Journal.getErreurs().getLog().length());
        CompteBancaire cb2 = new CompteBancaire(987654321);
        assertEquals(0,Journal.getOpérations().getLog().length());
        assertEquals(0,Journal.getErreurs().getLog().length());
        cb1.déposer(100);
        assertTrue(Journal.getOpérations().getLastMessage().contains("Dépôt de 100.0€ sur le compte 123456789."));
        cb2.retirer(10);
        assertTrue(Journal.getErreurs().getLastMessage().contains("La banque n'autorise pas de découvert (987654321)."));
        cb1.retirer(80);
        assertTrue(Journal.getOpérations().getLastMessage().contains("Retrait de 80.0€ sur le compte 123456789."));
        cb2.déposer(-1.33);
        assertTrue(Journal.getErreurs().getLastMessage().contains("Dépôt d'une valeur négative impossible (987654321)."));
                cb2.déposer(18.8);
        assertTrue(Journal.getOpérations().getLastMessage().contains("Dépôt de 18.8€ sur le compte 987654321."));
        cb1.retirer(-150.0);
        assertTrue(Journal.getErreurs().getLastMessage().contains("Retrait d'une valeur négative impossible (123456789)."));
    }
}

/*
Question subsidiaire:
On pourrait faire un manager qui stocke les instances de Journal (ce manager aurait lui même une instance),
ça permet de rendre la class Journal mpins grosse
 */

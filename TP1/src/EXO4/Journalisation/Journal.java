package EXO4.Journalisation;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class Journal {

	private static final Map<String, Journal> instances = new HashMap<String, Journal>();
	private StringBuffer log;
	private String last;

	private Journal() {
		this.log = new StringBuffer();
	}

	public void ajouterLog(String log) {
		this.last = log;
		Date d = new Date();
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH'h'mm'm'ss's'SSS");
		this.log.append("[" + dateFormat.format(d) + "] " + log + "\n");
	}

	public String getLastMessage() {
		return last;
	}

	@Override
	public String toString() {
		return this.log.toString();
	}

	public synchronized static Journal getInstance(String key) {
		if(instances.containsKey(key)) {
			return instances.get(key);
		}

		Journal journal = new Journal();
		instances.put(key, journal);
		return journal;
	}

	public StringBuffer getLog() {
		return log;
	}

	public synchronized static Journal getOpérations() {
		return getInstance("OPE");
	}

	public synchronized static Journal getErreurs() {
		return getInstance("ERR");
	}
}

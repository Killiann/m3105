package EXO3.Journalisation;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Journal {

	private static Journal accept, refuse;
	private StringBuffer log;

	private Journal() {
		this.log = new StringBuffer();
	}

	public void ajouterLog(String log) {
		Date d = new Date();
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH'h'mm'm'ss's'SSS");
		this.log.append("[" + dateFormat.format(d) + "] " + log + "\n");
	}

	@Override
	public String toString() {
		return this.log.toString();
	}

	public static Journal getAccept() {
		return accept == null ? accept = new Journal() : accept;
	}

	public static Journal getRefuse() {
		return refuse == null ? refuse = new Journal() : refuse;
	}
}

package EXO3.Singleton;

import org.junit.Assert;
import org.junit.Test;

public class SingletonTest {

    @Test
    public void testGetInstance1() {
        Assert.assertNotNull(Singleton.getInstance());
    }

    @Test
    public void testGetInstance2() {
        Assert.assertEquals(Singleton.getInstance().getClass(), Singleton.class);
    }

    @Test
    public void testGetInstance3() {
        Assert.assertEquals(Singleton.getInstance(), Singleton.getInstance());
    }
} 

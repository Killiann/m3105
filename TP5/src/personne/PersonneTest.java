package personne;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class PersonneTest {

    private Personne[] listePersonnes;
    private ObserverTest testObserv;

    @Before
    public void setup() {
        listePersonnes = new Personne[]{ new Personne("Sédes", "Florence"),
                new Personne("Viallet", "Fabienne"),
                new Personne("Julien", "Christine"),
                new Personne("Percebois", "Christian"),
                new Personne("Leblanc", "Hervé"),
                new Personne("TFCSIXZERO", "Killian"),
                new Personne("Troyes", "Fabio")};

        testObserv = new PersonneTest.ObserverTest();

        for (Personne personne : listePersonnes) {
            personne.attach(testObserv);
        }
    }

    @After
    public void tearTown() {
        listePersonnes = null;
        testObserv = null;
    }

    @Test
    public void testSetNom() {
        listePersonnes[0].setNom("TEST");
        Assert.assertTrue(testObserv.isUpdate());
    }

    @Test
    public void testAjoutPrénom() {
        listePersonnes[0].ajoutPrénom("TEST");
        Assert.assertTrue(testObserv.isUpdate());
    }

    public class ObserverTest implements personne.Observer {

        public boolean update = false;

        @Override
        public void update(Subject s) {
            update = true;
        }

        public boolean isUpdate() {
            return update;
        }
    }
}
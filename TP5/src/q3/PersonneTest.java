package q3;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Observable;
import java.util.Observer;

public class PersonneTest {

    private Personne[] listePersonnes;
    private ObserverTest testObserv;

    @Before
    public void setup() {
        listePersonnes = new Personne[]{ new Personne("Sédes", "Florence"),
                new Personne("Viallet", "Fabienne"),
                new Personne("Julien", "Christine"),
                new Personne("Percebois", "Christian"),
                new Personne("Leblanc", "Hervé"),
                new Personne("TFCSIXZERO", "Killian"),
                new Personne("Troyes", "Fabio")};

        testObserv = new ObserverTest();

        for (Personne personne : listePersonnes) {
            personne.addObserver(testObserv);
        }
    }

    @After
    public void tearTown() {
        listePersonnes = null;
        testObserv = null;
    }

    @Test
    public void testSetNom() {
        listePersonnes[0].setNom("TEST");
        Assert.assertTrue(testObserv.isUpdate());
    }

    @Test
    public void testAjoutPrénom() {
        listePersonnes[0].ajoutPrénom("TEST");
        Assert.assertTrue(testObserv.isUpdate());
    }

    public class ObserverTest implements Observer {

        public boolean update = false;

        public boolean isUpdate() {
            return update;
        }

        @Override
        public void update(Observable o, Object arg) {
            update = true;
        }
    }
}
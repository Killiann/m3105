package q3;

import java.util.Observable;
import java.util.Observer;

public class ObserverPTT implements Observer {

	@Override
	public void update(Observable s, Object arg) {
		Personne p = (Personne) s;
		if (!p.getAfter().getNom().equals(p.getBefore().getNom()))
			this.changementDeNom(p);
	}
	
	private void changementDeNom(Personne p) {
		System.out.println("Changement d'adresse postal pour " + p.getBefore().getNom() + " devient " + p.getAfter().getNom());
	}
}

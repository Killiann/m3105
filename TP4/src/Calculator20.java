import java.util.ArrayList;
import java.util.List;

public class Calculator20 extends Calculator {

    public Calculator20(EtatDistributeur etat) {
        super(etat, 20);
    }

    @Override
    protected List<Couple> calculate(int baseMontant, boolean gros) {
        List<Couple> proposition = new ArrayList<>();

        int nBillets20 = montant%20 == 0 && !gros ? (montant / 20 - 1) : (montant / 20);
        nBillets20 = Math.min(nBillets20, this.etat.getNb20Disponible());

        montant -= nBillets20*20;
        this.etat.setNb20Disponible(this.etat.getNb20Disponible() - nBillets20);

        proposition.add(new Couple(20, nBillets20));

        return proposition;
    }
}

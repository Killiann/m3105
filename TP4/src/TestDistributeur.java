import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class TestDistributeur {

    public static void main(String[] args) {
        Distributeur d = new Distributeur(10, 10, 10, 10);
        List<Couple> proposition = d.donnerBillets(10);
        System.out.println(d.toStringProposition(proposition, 10));

        d.recharger(10, 10, 10, 10);
        proposition = d.donnerBillets(20);
        System.out.println(d.toStringProposition(proposition, 20));

        d.recharger(10, 10, 10, 10);
        proposition = d.donnerBillets(30);
        System.out.println(d.toStringProposition(proposition, 30));

        d.recharger(10, 10, 10, 10);
        proposition = d.donnerBillets(40);
        System.out.println(d.toStringProposition(proposition, 40));

        d.recharger(10, 10, 10, 10);
        proposition = d.donnerBilletsPetitesCoupures(45);
        System.out.println(d.toStringProposition(proposition, 45));

        d.recharger(10, 10, 10, 10);
        proposition = d.donnerBillets(50);
        System.out.println(d.toStringProposition(proposition, 50));

        d.recharger(10, 10, 10, 10);
        proposition = d.donnerBillets(60);
        System.out.println(d.toStringProposition(proposition, 60));

        d.recharger(10, 10, 10, 10);
        proposition = d.donnerBillets(70);
        System.out.println(d.toStringProposition(proposition, 70));

        d.recharger(10, 10, 10, 10);
        proposition = d.donnerBillets(100);
        System.out.println(d.toStringProposition(proposition, 100));

        d.recharger(10, 10, 10, 10);
        proposition = d.donnerBillets(110);
        System.out.println(d.toStringProposition(proposition, 110));

        d.recharger(10, 10, 10, 10);
        proposition = d.donnerBillets(210);
        System.out.println(d.toStringProposition(proposition, 210));

        d.recharger(10, 10, 10, 10);
        proposition = d.donnerBillets(310);
        System.out.println(d.toStringProposition(proposition, 310));

        d.recharger(10, 10, 10, 10);
        proposition = d.donnerBillets(3000);
        System.out.println(d.toStringProposition(proposition, 3000));
    }

    private Distributeur distributeur;

    @Before
    public void setUp() throws Exception {
        distributeur = new Distributeur(10, 10, 10, 10);
    }

    @Test
    public void test10() {
        List<Couple> proposition = distributeur.donnerBillets(10);
        assertEquals(0, distributeur.montantRestantDe(proposition, 10));
        assertEquals(10, proposition.get(0).getValeurBillet());
        assertEquals(1, proposition.get(0).getNombreBilletsDelivres());
    }

    @Test
    public void test20() {
        List<Couple> proposition = distributeur.donnerBillets(20);
        assertEquals(0, distributeur.montantRestantDe(proposition, 20));
        assertEquals(10, proposition.get(0).getValeurBillet());
        assertEquals(2, proposition.get(0).getNombreBilletsDelivres());
    }

    @Test
    public void test30() {
        List<Couple> proposition = distributeur.donnerBillets(30);
        assertEquals(0, distributeur.montantRestantDe(proposition, 30));
        assertEquals(20, proposition.get(0).getValeurBillet());
        assertEquals(1, proposition.get(0).getNombreBilletsDelivres());
        assertEquals(10, proposition.get(1).getValeurBillet());
        assertEquals(1, proposition.get(1).getNombreBilletsDelivres());
    }

    @Test
    public void test40() {
        List<Couple> proposition = distributeur.donnerBillets(40);
        assertEquals(0, distributeur.montantRestantDe(proposition, 40));
        assertEquals(20, proposition.get(0).getValeurBillet());
        assertEquals(1, proposition.get(0).getNombreBilletsDelivres());
        assertEquals(10, proposition.get(1).getValeurBillet());
        assertEquals(2, proposition.get(1).getNombreBilletsDelivres());
    }

    @Test
    public void test45() {
        List<Couple> proposition = distributeur.donnerBilletsPetitesCoupures(45);
        assertEquals(0, distributeur.montantRestantDe(proposition, 45));
        assertEquals(20, proposition.get(0).getValeurBillet());
        assertEquals(2, proposition.get(0).getNombreBilletsDelivres());
        assertEquals(5, proposition.get(1).getValeurBillet());
        assertEquals(1, proposition.get(1).getNombreBilletsDelivres());
    }

    @Test
    public void test50() {
        List<Couple> proposition = distributeur.donnerBillets(50);
        assertEquals(0, distributeur.montantRestantDe(proposition, 50));
        assertEquals(20, proposition.get(0).getValeurBillet());
        assertEquals(2, proposition.get(0).getNombreBilletsDelivres());
        assertEquals(10, proposition.get(1).getValeurBillet());
        assertEquals(1, proposition.get(1).getNombreBilletsDelivres());
    }

    @Test
    public void test60() {
        List<Couple> proposition = distributeur.donnerBillets(60);
        assertEquals(0, distributeur.montantRestantDe(proposition, 60));
        assertEquals(20, proposition.get(0).getValeurBillet());
        assertEquals(2, proposition.get(0).getNombreBilletsDelivres());
        assertEquals(10, proposition.get(1).getValeurBillet());
        assertEquals(2, proposition.get(1).getNombreBilletsDelivres());
    }

    @Test
    public void test70() {
        List<Couple> proposition = distributeur.donnerBillets(70);
        assertEquals(0, distributeur.montantRestantDe(proposition, 70));
        assertEquals(20, proposition.get(0).getValeurBillet());
        assertEquals(3, proposition.get(0).getNombreBilletsDelivres());
        assertEquals(10, proposition.get(1).getValeurBillet());
        assertEquals(1, proposition.get(1).getNombreBilletsDelivres());
    }

    @Test
    public void test100() {
        List<Couple> proposition = distributeur.donnerBillets(100);
        assertEquals(0, distributeur.montantRestantDe(proposition, 100));
        assertEquals(50, proposition.get(0).getValeurBillet());
        assertEquals(1, proposition.get(0).getNombreBilletsDelivres());
        assertEquals(20, proposition.get(1).getValeurBillet());
        assertEquals(2, proposition.get(1).getNombreBilletsDelivres());
        assertEquals(10, proposition.get(2).getValeurBillet());
        assertEquals(1, proposition.get(2).getNombreBilletsDelivres());
    }

    @Test
    public void test110() {
        List<Couple> proposition = distributeur.donnerBillets(110);
        assertEquals(0, distributeur.montantRestantDe(proposition, 110));
        assertEquals(50, proposition.get(0).getValeurBillet());
        assertEquals(1, proposition.get(0).getNombreBilletsDelivres());
        assertEquals(20, proposition.get(1).getValeurBillet());
        assertEquals(2, proposition.get(1).getNombreBilletsDelivres());
        assertEquals(10, proposition.get(2).getValeurBillet());
        assertEquals(2, proposition.get(2).getNombreBilletsDelivres());
    }

    @Test
    public void test210() {
        List<Couple> proposition = distributeur.donnerBillets(210);
        assertEquals(0, distributeur.montantRestantDe(proposition, 210));
        assertEquals(50, proposition.get(0).getValeurBillet());
        assertEquals(2, proposition.get(0).getNombreBilletsDelivres());
        assertEquals(20, proposition.get(1).getValeurBillet());
        assertEquals(5, proposition.get(1).getNombreBilletsDelivres());
        assertEquals(10, proposition.get(2).getValeurBillet());
        assertEquals(1, proposition.get(2).getNombreBilletsDelivres());
    }

    @Test
    public void test310() {
        List<Couple> proposition = distributeur.donnerBillets(310);
        assertEquals(0, distributeur.montantRestantDe(proposition, 310));
        assertEquals(50, proposition.get(0).getValeurBillet());
        assertEquals(3, proposition.get(0).getNombreBilletsDelivres());
        assertEquals(20, proposition.get(1).getValeurBillet());
        assertEquals(7, proposition.get(1).getNombreBilletsDelivres());
        assertEquals(10, proposition.get(2).getValeurBillet());
        assertEquals(2, proposition.get(2).getNombreBilletsDelivres());
    }

    @Test
    public void test3000() {
        List<Couple> proposition = distributeur.donnerBillets(3000);
        assertEquals(2200, distributeur.montantRestantDe(proposition, 3000));
        assertEquals(50, proposition.get(0).getValeurBillet());
        assertEquals(10, proposition.get(0).getNombreBilletsDelivres());
        assertEquals(20, proposition.get(1).getValeurBillet());
        assertEquals(10, proposition.get(1).getNombreBilletsDelivres());
        assertEquals(10, proposition.get(2).getValeurBillet());
        assertEquals(10, proposition.get(2).getNombreBilletsDelivres());
    }

}

public class Wrapper {

    private int i;

    public Wrapper(int i) {
        this.i = i;
    }

    public void setValue(int i) {
        this.i = i;
    }

    public int getValue() {
        return i;
    }
}

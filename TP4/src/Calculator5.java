import java.util.ArrayList;
import java.util.List;

public class Calculator5 extends Calculator {

    public Calculator5(EtatDistributeur etat) {
        super(etat, 0);
    }

    @Override
    protected List<Couple> calculate(int baseMontant, boolean gros) {
        List<Couple> proposition = new ArrayList<>();

        if(gros || baseMontant >= 100 || baseMontant < 20) {
            return proposition;
        }

        int nBillets5 = Math.min(montant/5, this.etat.getNb5Disponible());
        montant -= nBillets5*5;
        this.etat.setNb10Disponible(this.etat.getNb5Disponible() - nBillets5);
        proposition.add(new Couple(5, nBillets5));

        return proposition;
    }
}

import java.util.ArrayList;
import java.util.List;

public class Calculator10 extends Calculator {

    // On ne peut jamais retirer 9€ dans un distributeur donc ça ne pause pas de pb
    public Calculator10(EtatDistributeur etat) {
        super(etat, 9);
    }

    @Override
    protected List<Couple> calculate(int baseMontant, boolean gros) {
        List<Couple> proposition = new ArrayList<>();

        int nBillets10 = Math.min(montant/10, this.etat.getNb10Disponible());
        montant -= nBillets10*10;
        this.etat.setNb10Disponible(this.etat.getNb10Disponible() - nBillets10);
        proposition.add(new Couple(10,nBillets10));

        return proposition;
    }
}

import java.util.ArrayList;
import java.util.List;

public class DistributeurConverteur {

    private Distributeur distributeur;
    private int montant;
    private int baseMontant;

    private List<Couple> proposition = new ArrayList<>();

    public DistributeurConverteur(Distributeur distributeur, int montant, boolean gros) {
        this.distributeur = distributeur;
        this.montant = montant;
        this.baseMontant = montant;
        converteur(gros);
    }

    private void converteur(boolean gros) {
        for (Calculator calculator : distributeur.getCalculators()) {
            if(this.montant <= 0) break;
            if(!gros ? this.montant > calculator.getId() : this.montant >= calculator.getId()) {
                proposition.addAll(calculator.getProposition(this.baseMontant, this.montant, gros));
                this.montant = calculator.getMontant();
            }
        }
    }

    public List<Couple> getProposition() {
        return proposition;
    }

    public int getBaseMontant() {
        return baseMontant;
    }
}

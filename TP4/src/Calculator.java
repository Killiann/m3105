import java.util.List;

public abstract class Calculator {

    protected int id;

    protected int montant;
    protected EtatDistributeur etat;

    public Calculator(EtatDistributeur etat, int id) {
        this.id = id;
        this.etat = etat;
    }

    public List<Couple> getProposition(int baseMontant, int montant, boolean gros){
        this.montant = montant;
        return calculate(baseMontant, gros);
    }

    protected abstract List<Couple> calculate(int baseMontant, boolean gros);

    public int getId() {
        return id;
    }

    public int getMontant() {
        return montant;
    }
}

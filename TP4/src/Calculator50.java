import java.util.ArrayList;
import java.util.List;

public class Calculator50 extends Calculator {

    public Calculator50(EtatDistributeur etat) {
        super(etat, 50);
    }

    @Override
    protected List<Couple> calculate(int baseMontant, boolean gros) {
        List<Couple> proposition = new ArrayList<>();

        int nBillets50 = Math.min((int)Math.ceil(montant/2/50), this.etat.getNb50Disponible());
        this.montant -= nBillets50*50;
        this.etat.setNb50Disponible(this.etat.getNb50Disponible() - nBillets50);
        if (nBillets50 > 0) {
            proposition.add(new Couple(50,nBillets50));
        }

        return proposition;
    }
}

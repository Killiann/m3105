import java.util.Arrays;
import java.util.List;

public class Distributeur {
	
	private EtatDistributeur etat;

	private List<Calculator> calculators;
	
	public Distributeur(int nb50, int nb20, int nb10, int nb5) {
		this.etat = new EtatDistributeur();
		recharger(nb50, nb20, nb10, nb5);

		calculators = Arrays.asList(new Calculator50(this.etat), new Calculator20(this.etat), new Calculator10(this.etat), new Calculator5(this.etat));
	}

	public void recharger(int nb50, int nb20, int nb10, int nb5) {
		this.etat.setNb50Disponible(nb50);
		this.etat.setNb20Disponible(nb20);
		this.etat.setNb10Disponible(nb10);
		this.etat.setNb5Disponible(nb5);
	}
	
	public List<Couple> donnerBillets(int montant) {
		return donnerBilletsPetitesCoupures(montant); // Methode de iut.test, changer en fonction de ce qu'on veut faire
    }

    public List<Couple> donnerBilletsPetitesCoupures(int montant) {
		return new DistributeurConverteur(this, montant, false).getProposition();
	}

	public List<Couple> donnerBilletsGrossesCoupures(int montant) {
		return new DistributeurConverteur(this, montant, true).getProposition();
	}

	public String toStringProposition(List<Couple> proposition, int montant) {
		StringBuffer res = new StringBuffer();
		res.append("Montant à débiter : " + montant + "€ \n");
		for (Couple c : proposition) {
			res.append(c.toString() + '\n');
		}
		res.append("Montant restant d€ : " + this.montantRestantDe(proposition, montant));
		return res.toString();
	}
	
	public int montantRestantDe(List<Couple> proposition, int montant) {
		int montantRestantDe = montant;
		for (Couple c : proposition) {
			montantRestantDe -= c.getValeurBillet() * c.getNombreBilletsDelivres();
		}
		return montantRestantDe;
	}

	public List<Calculator> getCalculators() {
		return calculators;
	}
}

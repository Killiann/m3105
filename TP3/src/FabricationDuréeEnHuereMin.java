public class FabricationDuréeEnHuereMin implements FabricationDuree {

    @Override
    public Duree create(int heures, int minutes, int secondes) throws IllegalArgumentException {
        return new DureeEnHeuresMinutesSecondes(heures, minutes, secondes);
    }
}

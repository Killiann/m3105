import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;


public abstract class TestDuree {

	protected FabricationDuree fabricationDuree;

	@Before
	public abstract void setUp();

	@After
	public void tearDown() {
		fabricationDuree = null;
	}

	@Test
	public void testGetters() {
		Duree d = fabricationDuree.create(1, 2, 3);
		assertEquals(1,d.getHeures());
		assertEquals(2,d.getMinutes());
		assertEquals(3,d.getSecondes());
	}

	@Test (expected=IllegalArgumentException.class)
	public void testHeuresNegative() {
		fabricationDuree.create(-1, 2, 3);
	}
	
	@Test (expected=IllegalArgumentException.class)
	public void testMinutesNegative() {
		fabricationDuree.create(1, -2, 3);
	}
	
	@Test (expected=IllegalArgumentException.class)
	public void testMinutesSuperieur59() {
		fabricationDuree.create(1, 60, 3);
	}
	
	@Test (expected=IllegalArgumentException.class)
	public void testSecondesNegative() {
		fabricationDuree.create(1, 2, -3);
	}
	
	@Test (expected=IllegalArgumentException.class)
	public void testSecondesSuperieur59() {
		fabricationDuree.create(1, 2, 60);
	}
	
	@Test
	public void testEqualsValeursEgales() {
		assertEquals(fabricationDuree.create(1,2,3),fabricationDuree.create(1,2,3));
	}
	
	@Test
	public void testEqualsValeursNonEgales() {
		assertNotEquals(fabricationDuree.create(1,2,3),fabricationDuree.create(2,2,3));
		assertNotEquals(fabricationDuree.create(1,2,3),fabricationDuree.create(1,1,3));
		assertNotEquals(fabricationDuree.create(1,2,3),fabricationDuree.create(1,2,2));
	}
	
	@Test
	public void testAjouterUneSeconde() {
		Duree d123 = fabricationDuree.create(1,2,3);
		d123.ajouterUneSeconde();
		assertEquals(fabricationDuree.create(1,2,4),d123);
		
		Duree d1259 = fabricationDuree.create(1,2,59);
		d1259.ajouterUneSeconde();
		assertEquals(fabricationDuree.create(1,3,0),d1259);
		
		Duree d5959 = fabricationDuree.create(1,59,59);
		d5959.ajouterUneSeconde();
		assertEquals(fabricationDuree.create(2,0,0),d5959);
	}


	@Test
	public void testAjouterUneMinure() {
		Duree d123 = fabricationDuree.create(1,2,3);
		d123.ajouterUneMinute();
		assertEquals(fabricationDuree.create(1,3,3),d123);

		Duree d1259 = fabricationDuree.create(1,59,59);
		d1259.ajouterUneMinute();
		assertEquals(fabricationDuree.create(2,0,59),d1259);
	}

	@Test
	public void testAjouterUneHeure() {
		Duree d123 = fabricationDuree.create(1,2,3);
		d123.ajouterUneHeure();
		assertEquals(fabricationDuree.create(2,2,3),d123);
	}
	
	@Test
	public void testCompareTo() {
		assertTrue(fabricationDuree.create(1,2,4).compareTo(fabricationDuree.create(1,0,0)) > 0);
		assertEquals(0,fabricationDuree.create(1,2,4).compareTo(fabricationDuree.create(1,2,4)));
		assertTrue(fabricationDuree.create(1,0,0).compareTo(fabricationDuree.create(1,2,4)) < 0);
	}
	
	@Test 
	public void testToString() {
		assertEquals("1:2:3", fabricationDuree.create(1,2,3).toString());
	}

}

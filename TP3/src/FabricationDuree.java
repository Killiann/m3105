public interface FabricationDuree {

    Duree create(int heures, int minutes, int secondes) throws IllegalArgumentException;
}

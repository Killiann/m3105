public class FabricationDuréeSecondes implements FabricationDuree {

    @Override
    public Duree create(int heures, int minutes, int secondes) throws IllegalArgumentException {
        return new DureeEnSecondes(heures, minutes, secondes);
    }
}

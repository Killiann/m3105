public interface Duree extends Comparable<Duree> {

	int getHeures();

	int getMinutes();

	int getSecondes();

	void ajouterUneSeconde();
	void ajouterUneHeure();
	void ajouterUneMinute();
}